const fs = require('fs');
const ejs = require('ejs');

function renderTemplate(templateName) {
  ejs.renderFile(
    `${__dirname}/ejs/${templateName}.ejs`,
    {
      PROTOCOL: process.env.PROTOCOL,
      DOMAIN: process.env.DOMAIN,
      APP_ID: process.env.APP_ID,
    },
    {},
    (err, result) =>
      fs.writeFileSync(`${__dirname}/../public/${templateName}.html`, result)
  );
}

function renderTemplates() {
  renderTemplate('editing');
  renderTemplate('login');
  renderTemplate('logout');
  renderTemplate('index');
}

module.exports = {
  renderTemplates,
};
