#!/usr/bin/env node
require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const ParseServer = require('parse-server/lib/ParseServer').default;
const path = require('path');
const words = require('./words.json');
const { renderTemplates } = require('./renderTemplates');

renderTemplates();

if (!process.env.DATABASE_URI) {
  console.log('DATABASE_URI not specified, falling back to localhost.');
}

const server = new ParseServer({
  databaseURI: process.env.DATABASE_URI || 'mongodb://localhost:27017/dev',
  cloud: __dirname + '/cloud/main.js',
  masterKey: process.env.MASTER_KEY,
  appId: process.env.APP_ID,
  serverURL: process.env.DOMAIN
    ? `${process.env.PROTOCOL}${process.env.DOMAIN}/parse`
    : 'http://localhost:1337/parse',
  liveQuery: {
    classNames: ['Document', 'Step'], // List of classes to support for query subscriptions
  },
});

const api = server.app;

const app = express();
const jsonParser = bodyParser.json();

// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '../public')));

// Serve the Parse API on the /parse URL prefix
const mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

function compareReads(currentResults, clientResults) {
  if (!Array.isArray(currentResults)) {
    currentResults = currentResults ? [currentResults] : [];
  }
  if (currentResults.length !== clientResults.length) {
    return false;
  }
  for (let i = 0; i < currentResults.length; i++) {
    if (
      currentResults[i].get('updatedAt').toString() !==
      clientResults[i].updatedAt
    ) {
      return false;
    }
  }
  return true;
}

app.post('/transaction', jsonParser, async (req, res) => {
  const dbController = server.config.databaseController;
  if (dbController._transactionalSession) {
    return res.send({ success: false });
  }
  try {
    await dbController.createTransactionalSession();
    const currentResultSets = await Promise.all(
      req.body.reads.map(({ query, className, type }) =>
        Parse.Query.fromJSON(className, query)[type]({
          sessionToken: req.body.sessionToken,
        })
      )
    );
    if (
      currentResultSets.every((currentResults, i) =>
        compareReads(currentResults, req.body.reads[i].results)
      )
    ) {
      const finalWrites = req.body.writes.map((write) =>
        Parse.Object.fromJSON(write)
      );
      await Promise.all(
        finalWrites.map((writeObject) =>
          writeObject.save(writeObject.attributes, {
            sessionToken: req.body.sessionToken,
          })
        )
      );
      await dbController.commitTransactionalSession();
      return res.send({ success: true });
    }
    await dbController.abortTransactionalSession();
    return res.send({ success: false });
  } catch (e) {
    console.error(e);
    if (!e.message.startsWith('There is no transactional session to')) {
      await dbController.abortTransactionalSession();
    }
    res.send({ success: false });
  }
});

app.get('/editing/*', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/editing.html'));
});

app.get('/create', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/editing.html'));
});

app.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/login.html'));
});

app.get('/logout', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/logout.html'));
});

app.get('/words', (reg, res) => {
  const wordList = [];
  for (let i = 0; i < 4; i++) {
    wordList.push(words[Math.round(Math.random() * words.length)]);
  }
  res.send(wordList.join('.'));
});

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/index.html'));
});

const port = process.env.PORT || 1337;
const httpServer = require('http').createServer(app);
httpServer.listen(port, () => {
  console.log('parse-server-example running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);
