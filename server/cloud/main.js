const { Step } = require('prosemirror-transform');
const { documentSchema } = require('../../schema');

Parse.Cloud.afterSave('Step', async (req) => {
  const currentParseStep = req.object;
  const parseDoc = await new Parse.Query('Document').get(
    currentParseStep.get('doc').id,
    { useMasterKey: true }
  );
  const newStepsQuery = new Parse.Query('Step');
  newStepsQuery.equalTo('doc', parseDoc);
  newStepsQuery.greaterThan('stepId', parseDoc.get('lastStepId') || 0);
  newStepsQuery.addAscending('stepId');
  newStepsQuery.limit(10000);
  const newSteps = await newStepsQuery.find({ useMasterKey: true });
  const updatedDoc = newSteps.reduce((acc, parseStep) => {
    if (!acc) {
      return;
    }
    const step = Step.fromJSON(documentSchema, parseStep.get('stepJSON'));
    const { doc, failed } = step.apply(acc);
    if (failed) {
      console.error(
        `Failed to apply step ${parseStep.get('stepId')} to document ${
          parseDoc.id
        }`
      );
      return;
    }
    return doc;
  }, documentSchema.nodeFromJSON(parseDoc.get('docJSON')));
  if (!updatedDoc) {
    return;
  }
  parseDoc.save(
    {
      docJSON: updatedDoc.toJSON(),
      lastStepId: newSteps[newSteps.length - 1].get('stepId'),
    },
    { useMasterKey: true }
  );
});
