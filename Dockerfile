FROM node:12 as builder

WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN npx pinst --disable
RUN yarn --frozen-lockfile
COPY . .

FROM node:12-slim
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app .
RUN yarn build
ENTRYPOINT [ "yarn" ]
CMD [ "start" ]
