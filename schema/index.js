const { Schema } = require('prosemirror-model');

module.exports = {
  documentSchema: new Schema({
    nodes: {
      doc: {
        content: 'block+',
      },
      paragraph: {
        content: 'inline*',
        group: 'block',
        parseDOM: [{ tag: 'p' }],
        toDOM() {
          return ['p', 0];
        },
      },
      list_item: {
        content: 'block|(block (ordered_list|unordered_list))',
        parseDOM: [{ tag: 'li' }],
        toDOM() {
          return ['li', 0];
        },
      },
      ordered_list: {
        content: 'list_item+',
        group: 'block',
        attrs: {
          test: { default: 'test' },
        },
        parseDOM: [{ tag: 'ol' }],
        toDOM() {
          return ['ol', 0];
        },
      },
      unordered_list: {
        content: 'list_item+',
        group: 'block',
        parseDOM: [{ tag: 'ul' }],
        toDOM() {
          return ['ul', 0];
        },
      },
      heading1: {
        content: 'inline*',
        group: 'block',
        parseDOM: [{ tag: 'h1' }],
        toDOM() {
          return ['h1', 0];
        },
      },
      heading2: {
        content: 'inline*',
        group: 'block',
        parseDOM: [{ tag: 'h2' }],
        toDOM() {
          return ['h2', 0];
        },
      },
      heading3: {
        content: 'inline*',
        group: 'block',
        parseDOM: [{ tag: 'h3' }],
        toDOM() {
          return ['h3', 0];
        },
      },
      text: {
        group: 'inline',
      },
    },
    marks: {
      // :: MarkSpec An emphasis mark. Rendered as an `<em>` element.
      // Has parse rules that also match `<i>` and `font-style: italic`.
      em: {
        parseDOM: [{ tag: 'i' }, { tag: 'em' }, { style: 'font-style=italic' }],
        toDOM() {
          return ['em', 0];
        },
      },

      // :: MarkSpec A strong mark. Rendered as `<strong>`, parse rules
      // also match `<b>` and `font-weight: bold`.
      strong: {
        parseDOM: [
          { tag: 'strong' },
          // This works around a Google Docs misbehavior where
          // pasted content will be inexplicably wrapped in `<b>`
          // tags with a font-weight normal.
          {
            tag: 'b',
            getAttrs: (node) => node.style.fontWeight != 'normal' && null,
          },
          {
            style: 'font-weight',
            getAttrs: (value) =>
              /^(bold(er)?|[5-9]\d{2,})$/.test(value) && null,
          },
        ],
        toDOM() {
          return ['strong', 0];
        },
      },
      // :: MarkSpec A link. Has `href` and `title` attributes. `title`
      // defaults to the empty string. Rendered and parsed as an `<a>`
      // element.
      link: {
        attrs: {
          href: {},
          title: { default: null },
        },
        inclusive: false,
        parseDOM: [
          {
            tag: 'a[href]',
            getAttrs: (dom) => ({
              href: dom.getAttribute('href'),
              title: dom.getAttribute('title'),
            }),
          },
        ],
        toDOM(node) {
          let { href, title } = node.attrs;
          return ['a', { href, title }, 0];
        },
      },
    },
  }),
};
