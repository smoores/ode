import { Schema } from 'prosemirror-model';

declare module './' {
  export type DocumentSchema = Schema<
    'doc' | 'paragraph' | 'heading1' | 'heading2' | 'heading3' | 'text',
    'em' | 'strong' | 'link'
  >;
  export const documentSchema: DocumentSchema;
}
