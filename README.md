# Ode

Ode is a self-hosted collaborative rich document editor.

It's based on [Parse Platform](https://parseplatform.org/), an open source, self-hosted "Complete Application Stack," and [Prosemirror](https://prosemirror.net), a rich text editor toolkit.

## Self-hosting

Ode's primary goal is to be as easy as possible to run on hardware that you own or rent. There are several ways to do this.

### Docker Containers (Manual)

Docker images for Ode are published to the GitLab Container Registry with every release. These images can be pulled from registry.gitlab.com/smoores/ode.

Ode requires an existing MongoDB ^4.2 installation. We recommend using the `percona/percona-server-mongodb:4.2` image.

```console
docker run --name mongodb -d -v mongodb_data:/data/db -p 27017:27017 percona/percona-server-mongodb:4.2
```

**Note**: The above example uses a [named volume](https://docs.docker.com/storage/volumes/) to store your data. This makes Docker responsible for managing your data's location. Use a path to a host directory instead of `mongodb_data` to instead use a [bind mount](https://docs.docker.com/storage/bind-mounts/), which makes your data available available on your host machine.

See the [Setting up your environment](#Setting%20up%20your%20environment) section for instructions on how to create a .env file.

To pull and run the latest version of Ode:

```console
docker run --name ode -d -p 1337:1337 --env-file .env registry.gitlab.com/smoores/ode:latest
```

#### Networking Tips

If you're running your MongoDB instance on the same physical machine as your Ode instance, you can create a Docker network to connect the two.

```console
docker network create --driver bridge ode-net
```

Now, when running your MongoDB and Ode containers:

```console
docker run --name mongodb -d -v mongodb_data:/data/db -p 27017:27017 --network ode-net percona/percona-server-mongodb:4.2

docker run --name ode -d -p 1337:1337 --env-file .env --network ode-net -e DATABASE_URI=mongodb://mongodb:27017 registry.gitlab.com/smoores/ode:latest
```

In addition to specifying the new network with the `--network` flag, we also set the `DATABASE_URI` environment variable to `mongodb://mongodb:27017`. Since the containers are on the same Docker network, they can resolve each others' IP addresses via container name.

### Docker Compose

The `docker-compose.yml` file in this repository is for development and _should not_ be used for a production instance. Instead, if you wish to use docker-compose to run your production instance, you can use the following:

```yaml
version: "3.7"

volumes:
  mongodb_data:

services:
  ode:
    container_name: ode
    image: 'registry.gitlab.com/smoores/ode:latest'
    env_file: .env
    ports:
      - 1337:1337
  mongodb:
    container_name: mongodb
    image: 'percona/percona-server-mongodb:4.2'
    restart: always
    command: '--journal --setParameter internalQueryExecYieldPeriodMS=1000 --setParameter internalQueryExecYieldIterations=100000'
    ports:
      - 27017:27017
    volumes:
      - mongodb_data:/data/db
```

**Note**: See the manual [Docker instructions](#Docker%20Containers%20(Manual)) above for instructions on generating the `.env` file.

### NPM

The built files are also published as an NPM package, [@ode/ode](https://www.npmjs.com/package/@ode/ode).

Install the package

```console
npm i -g @ode/ode
```

or

```console
yarn global add @ode/ode
```

See the [Setting up your environment](#Setting%20up%20your%20environment) section for instructions on how to create a .env file. Since you've installed Ode globally as an NPM package, you can just use the new `generateid` command to create the initial `.env` file.

From the same directory as the new `.env` file, run `ode` to start the web server.

### Setting up your environment

Ode requires an APP_ID and MASTER_KEY to run. It's best to keep these persistent across restarts, so we recommend saving them in a `.env` file. This repo has a script that will generate these values for you:

```console
./scripts/generateid.sh
```

If you have `npm` available in your environment, you can also run this with `npx`:

```console
npx -p @ode/ode generateid
```

Or, if you installed Ode globally with `npm` or `yarn`:

```console
generateid
```

The following environment variables are also required. You can add them to the `.env` file generated by the above script, or pass them as additional arguments to the `docker run` command below with the `-e` flag.

| env variable | example value | description |
|--------------|---------------|-------------|
| PROTOCOL     | https://      | The protocol to use when connecting to your server from the browser. This should always be `https://` unless you are only serving local traffic. |
| DOMAIN       | example.com   | The domain name to use when connecting to your server from the browser. |
| DATABASE_URI | mongodb://my.db.ip.address:27017 | The URI for your MongoDB instance. Remember that this will be accessed from within a docker container, so MongoDB will be unavailable at `localhost` even if running on the same machine. See [Networking Tips](#Networking%20Tips) for more information. |

## Local Development

The best and easiest way to run Ode locally is with the `docker-compose.yml` file in this repo, which is designed for local development (do not use it for production!).

```console
docker-compose up -d --build
```

This will spin up two containers; one running the Ode web server, and another running a Percona-based MongoDB instance. The MongoDB data is in a named volume; you can clear it with:

```console
docker volume rm ode_mongodb_data
```

Any changes to the client will rebuild the client bundle, and changes to the server will restart the server.