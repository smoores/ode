#!/usr/bin/env bash
function escape () {
  local value=$1
  echo "$value" | sed -e 's/[\/&]/\\&/g'
}

app_id=$(openssl rand -base64 12)
master_key=$(openssl rand -base64 36)
echo "APP_ID=$(escape $app_id)" > .env
echo "MASTER_KEY=$(escape $master_key)" >> .env
