import { InputRule } from 'prosemirror-inputrules';

export const inputReplacementRules = [
  new InputRule(
    /[‘'\u2018]([\d]{2}s?|til|tis|twas|round|bout)/gi,
    (state, match, start, end) => {
      const marks = state.doc.resolve(start).marks();
      const node = state.schema.text(`’${match[1]}`, marks);
      return state.tr.replaceWith(start, end, node);
    }
  ),
  new InputRule(/(^|[\s{[(<'"\u2018\u201C])'/gi, (state, match, start, end) => {
    const marks = state.doc.resolve(start).marks();
    const node = state.schema.text(`${match[1]}‘`, marks);
    return state.tr.replaceWith(start, end, node);
  }),
  new InputRule(/'/gi, (state, match, start, end) => {
    const marks = state.doc.resolve(start).marks();
    const node = state.schema.text('’', marks);
    return state.tr.replaceWith(start, end, node);
  }),
  new InputRule(/(^|[\s{[(<'"\u2018\u201C])"/gi, (state, match, start, end) => {
    const marks = state.doc.resolve(start).marks();
    const node = state.schema.text(`${match[1]}“`, marks);
    return state.tr.replaceWith(start, end, node);
  }),
  new InputRule(/"/gi, (state, match, start, end) => {
    const marks = state.doc.resolve(start).marks();
    const node = state.schema.text('”', marks);
    return state.tr.replaceWith(start, end, node);
  }),
  new InputRule(/--/gi, (state, match, start, end) => {
    const marks = state.doc.resolve(start).marks();
    const node = state.schema.text('—', marks);
    return state.tr.replaceWith(start, end, node);
  }),
  new InputRule(/\.{3}/gi, (state, match, start, end) => {
    const marks = state.doc.resolve(start).marks();
    const node = state.schema.text('…', marks);
    return state.tr.replaceWith(start, end, node);
  }),
  new InputRule(/\u2028/g, (state, match, start, end) => {
    const marks = state.doc.resolve(start).marks();
    const node = state.schema.text(' ', marks);
    return state.tr.replaceWith(start, end, node);
  }),
];
