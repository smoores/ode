import { ResolvedPos } from 'prosemirror-model';

export function resolvedNode($pos: ResolvedPos) {
  if ($pos.parent.childCount <= $pos.index()) {
    return $pos.parent;
  }
  return $pos.parent.child($pos.index());
}
