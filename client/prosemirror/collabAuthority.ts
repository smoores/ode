import { Step } from 'prosemirror-transform';
import { Node } from 'prosemirror-model';
import { DocumentSchema } from '../../schema';

export class CollabAuthority {
  private steps: Array<Step<DocumentSchema>> = [];
  private stepClientIDs: Array<string | number> = [];
  private onNewSteps: Array<() => void> = [];
  constructor(
    public doc: Node<DocumentSchema>,
    private initialVersion: number
  ) {
    console.debug('Initialized authority with initial version', initialVersion);
  }

  get version() {
    return this.steps.length + this.initialVersion;
  }

  addCallback(f: () => void) {
    this.onNewSteps.push(f);
  }

  receiveSteps(
    version: number,
    steps: Array<Step<DocumentSchema>>,
    clientIds: string[]
  ) {
    if (version !== this.initialVersion + this.steps.length + 1) {
      return;
    }
    // Apply and accumulate new steps
    steps.forEach((step, i) => {
      const stepResult = step.apply(this.doc);
      if (!stepResult.doc) {
        throw new Error(
          `Could not apply step ${step.toJSON()} to doc ${this.doc.toJSON()}`
        );
      }
      this.doc = stepResult.doc;
      this.steps.push(step);
      this.stepClientIDs.push(clientIds[i]);
    });
    // Signal listeners
    this.onNewSteps.forEach((f) => f());
  }

  stepsSince(version: number) {
    return {
      steps: this.steps.slice(version - this.initialVersion),
      clientIDs: this.stepClientIDs.slice(version - this.initialVersion),
    };
  }
}
