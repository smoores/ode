import { collab, getVersion, receiveTransaction } from 'prosemirror-collab';
import {
  baseKeymap,
  chainCommands,
  createParagraphNear,
  liftEmptyBlock,
  newlineInCode,
  setBlockType,
  splitBlock,
  toggleMark,
} from 'prosemirror-commands';
import { history, redo, undo } from 'prosemirror-history';
import { keymap } from 'prosemirror-keymap';
import { Node } from 'prosemirror-model';
import { EditorState } from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';
import * as uuid4 from 'uuid/v4';
import { Semaphore } from 'prex';
import { documentSchema, DocumentSchema } from '../../schema';
import { queryForNewSteps, writeSteps } from '../parseApi/editing';
import { CollabAuthority } from '../prosemirror/collabAuthority';
import { inputRules } from 'prosemirror-inputrules';
import { inputReplacementRules } from './inputRules';
import toolbarPlugin from './plugins/toolbar';
import linkInspectorPlugin from './plugins/linkInspector';
import {
  liftListItem,
  splitListItem,
  sinkListItem,
} from './commands/listCommands';

const clientID = uuid4();

let view: EditorView;
let state: EditorState;
let collabAuthority: CollabAuthority;

export function initializeEditorState(
  doc?: Node<DocumentSchema>,
  version?: number
) {
  if (doc && version !== undefined) {
    collabAuthority = new CollabAuthority(doc, version);
  }
  state = EditorState.create({
    schema: documentSchema,
    doc: collabAuthority && collabAuthority.doc,
    plugins: [
      history(),
      keymap({
        ...baseKeymap,
        Enter: chainCommands(
          newlineInCode,
          createParagraphNear,
          splitListItem,
          liftListItem,
          liftEmptyBlock,
          splitBlock
        ),
        'Mod-z': undo,
        'Mod-y': redo,
        'Mod-b': toggleMark(documentSchema.marks.strong),
        'Mod-i': toggleMark(documentSchema.marks.em),
        'Mod-Alt-0': setBlockType(documentSchema.nodes.paragraph),
        'Mod-Alt-1': setBlockType(documentSchema.nodes.heading1),
        'Mod-Alt-2': setBlockType(documentSchema.nodes.heading2),
        'Mod-Alt-3': setBlockType(documentSchema.nodes.heading3),
        'Shift-Tab': liftListItem,
        Tab: sinkListItem,
      }),
      collab({ version: collabAuthority && collabAuthority.version, clientID }),
      inputRules({ rules: inputReplacementRules }),
      toolbarPlugin,
      linkInspectorPlugin,
    ],
  });
  return state;
}

export function initializeEditorView(
  doc: Node<DocumentSchema>,
  version?: number
) {
  const state = initializeEditorState(doc, version);
  const mutex = new Semaphore(1);

  view = new EditorView(document.querySelector<HTMLElement>('article')!, {
    state,
    async dispatchTransaction(tr) {
      const newState = this.state.apply(tr);
      this.updateState(newState);
      await mutex.wait();
      const wroteSteps = await writeSteps(this.state);
      if (wroteSteps) {
        await queryForNewSteps();
      }
      mutex.release();
    },
  });

  collabAuthority.addCallback(() => {
    const newData = collabAuthority.stepsSince(getVersion(view.state));
    view.dispatch(
      receiveTransaction(view.state, newData.steps, newData.clientIDs)
    );
  });

  window.view = view;
  return view;
}

export function getEditorView() {
  if (!view) {
    throw new Error(
      'Must call initializeEditorView before accessing editorView'
    );
  }
  return view;
}

export function getCollabAuthority() {
  if (!collabAuthority) {
    throw new Error(
      'Must call initializeEditorState before accessing collabAuthority'
    );
  }
  return collabAuthority;
}
