import {
  wrapInList,
  liftListItem as liftListItemFactory,
  splitListItem as splitListItemFactory,
  sinkListItem as sinkListItemFactory,
} from 'prosemirror-schema-list';
import { documentSchema } from '../../../schema';

export const wrapInOrderedList = wrapInList(documentSchema.nodes.ordered_list);

export const liftListItem = liftListItemFactory(documentSchema.nodes.list_item);
export const splitListItem = splitListItemFactory(
  documentSchema.nodes.list_item
);
export const sinkListItem = sinkListItemFactory(documentSchema.nodes.list_item);

export const wrapInUnorderedList = wrapInList(
  documentSchema.nodes.unordered_list
);
