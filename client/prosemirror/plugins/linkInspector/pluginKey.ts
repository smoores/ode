import { ResolvedPos, Node as ProsemirrorNode } from 'prosemirror-model';
import { PluginKey, SelectionRange } from 'prosemirror-state';
import { DecorationSet } from 'prosemirror-view';
import { DocumentSchema } from '../../../../schema';

export interface LinkInspectorPluginMeta {
  newLink?: {
    $start: ResolvedPos<DocumentSchema>;
    $end: ResolvedPos<DocumentSchema>;
  };
  selectedNode?: ProsemirrorNode<DocumentSchema>;
}

export interface LinkInspectorPluginState {
  newLinkRange?: SelectionRange<DocumentSchema>;
  inspectedLinkNode?: ProsemirrorNode<DocumentSchema>;
  set: DecorationSet<DocumentSchema>;
}

export const linkInspectorPluginKey = new PluginKey<
  LinkInspectorPluginState,
  DocumentSchema
>('linkInspector');
