import { Plugin, SelectionRange } from 'prosemirror-state';
import { documentSchema, DocumentSchema } from '../../../../schema';
import {
  linkInspectorPluginKey,
  LinkInspectorPluginMeta,
  LinkInspectorPluginState,
} from './pluginKey';
import { resolvedNode } from '../../primitives';
import { Decoration, DecorationSet, EditorView } from 'prosemirror-view';
import {
  createEditLinkInspector,
  createNewLinkInspector,
} from '../../../domManagement/links';

function handleSelectionChange(
  editorView: EditorView<DocumentSchema>,
  event: MouseEvent | KeyboardEvent
) {
  const selectedNode = resolvedNode(editorView.state.selection.$anchor);
  if (
    documentSchema.marks.link.isInSet(selectedNode.marks) &&
    !linkInspectorPluginKey.getState(editorView.state)!.inspectedLinkNode
  ) {
    editorView.dispatch(
      editorView.state.tr.setMeta(linkInspectorPluginKey, {
        selectedNode,
      })
    );
  } else if (
    (linkInspectorPluginKey.getState(editorView.state)!.inspectedLinkNode ||
      linkInspectorPluginKey.getState(editorView.state)!.newLinkRange) &&
    !event
      .composedPath()
      .some((e) => (e as HTMLElement).dataset?.linkInspectorTarget)
  ) {
    editorView.dispatch(
      editorView.state.tr.setMeta(linkInspectorPluginKey, {})
    );
  }
}

const linkInspectorPlugin = new Plugin<
  LinkInspectorPluginState,
  DocumentSchema
>({
  key: linkInspectorPluginKey,
  state: {
    init() {
      return { set: DecorationSet.empty };
    },
    apply(
      tr,
      { newLinkRange, inspectedLinkNode, set }
    ): LinkInspectorPluginState {
      const linkMeta = tr.getMeta(
        linkInspectorPluginKey
      ) as LinkInspectorPluginMeta;
      if (!linkMeta) {
        return {
          ...(newLinkRange && {
            newLinkRange: new SelectionRange(
              tr.doc.resolve(tr.mapping.map(newLinkRange.$from.pos)),
              tr.doc.resolve(tr.mapping.map(newLinkRange.$to.pos))
            ),
          }),
          ...(inspectedLinkNode && { inspectedLinkNode }),
          set: set.map(tr.mapping, tr.doc),
        };
      }

      if (linkMeta.newLink) {
        const newLinkRange = new SelectionRange(
          linkMeta.newLink.$start,
          linkMeta.newLink.$end
        );
        return {
          newLinkRange,
          set: DecorationSet.create(tr.doc, [
            Decoration.widget(
              linkMeta.newLink.$start.pos,
              (editorView, getPos) =>
                createNewLinkInspector(editorView, getPos, () => {
                  const currentNewLinkRange = linkInspectorPlugin.getState(
                    editorView.state
                  ).newLinkRange;
                  return currentNewLinkRange
                    ? currentNewLinkRange.$to.pos -
                        currentNewLinkRange.$from.pos
                    : 0;
                })
            ),
          ]),
        };
      }

      if (linkMeta.selectedNode) {
        return {
          inspectedLinkNode: linkMeta.selectedNode,
          set: DecorationSet.create(tr.doc, [
            Decoration.widget(
              tr.selection.anchor - tr.selection.$anchor.textOffset,
              (editorView, getPos) =>
                createEditLinkInspector(
                  editorView,
                  getPos,
                  () =>
                    resolvedNode(editorView.state.selection.$anchor).nodeSize,
                  linkMeta.selectedNode!.marks.find(
                    (mark) => mark.type === documentSchema.marks.link
                  )!.attrs.href
                )
            ),
          ]),
        };
      }

      return { set: DecorationSet.empty };
    },
  },
  props: {
    decorations(state) {
      return linkInspectorPluginKey.getState(state)!.set;
    },
    handleDOMEvents: {
      click(editorView, event) {
        handleSelectionChange(editorView, event);
        return true;
      },
      keyup(editorView, event) {
        handleSelectionChange(editorView, event);
        return true;
      },
      submit(editorView) {
        if (this.getState(editorView.state).newLinkRange) {
          editorView.dispatch(
            editorView.state.tr.setMeta(linkInspectorPluginKey, {})
          );
        }
        return true;
      },
    },
  },
});

export default linkInspectorPlugin;
