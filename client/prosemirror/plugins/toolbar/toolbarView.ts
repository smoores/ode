import { toggleMark, setBlockType } from 'prosemirror-commands';
import { documentSchema, DocumentSchema } from '../../../../schema';
import { toolbarPluginKey } from './pluginKey';
import { MarkType, NodeType, Node as ProsemirrorNode } from 'prosemirror-model';
import { EditorState } from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';
import { linkInspectorPluginKey } from '../linkInspector/pluginKey';
import {
  wrapInOrderedList,
  wrapInUnorderedList,
} from '../../commands/listCommands';

export function markIsActive(
  type: MarkType<DocumentSchema>,
  editorState: EditorState<DocumentSchema> | null
) {
  if (!editorState) {
    return false;
  }
  const { from, $from, to, empty } = editorState.selection;
  if (empty) {
    // Casting this to a boolean because it returns undefined when not truthy
    return type.isInSet(editorState.storedMarks || $from.marks()) != null;
  }
  return editorState.doc.rangeHasMark(from, to, type);
}

export function markIsDisabled(
  type: MarkType<DocumentSchema>,
  editorState: EditorState<DocumentSchema>
) {
  const { $from, empty } = editorState.selection;
  if (empty) {
    return !$from.parent.type.allowsMarkType(type);
  }
  const { content } = editorState.selection.content();
  // If content is selected, only disable the mark if it is disallowed in every node
  const children: ProsemirrorNode[] = [];
  content.forEach((child) => children.push(child));
  return children.every((node) => !node.type.allowsMarkType(type));
}

export function blockIsActive(
  type: NodeType<DocumentSchema>,
  editorState: EditorState<DocumentSchema> | null
) {
  if (!editorState) {
    return false;
  }
  const { $from, to } = editorState.selection;
  return to <= $from.end() && $from.parent.hasMarkup(type);
}

export default class ToolbarView {
  toolbar: HTMLElement;
  markButtons: NodeListOf<HTMLElement>;
  typeButtons: NodeListOf<HTMLElement>;

  preventFocus: (e: Event) => void;
  constructor(editorView: EditorView<DocumentSchema>) {
    this.toolbar = document.querySelector('[data-toolbar]') as HTMLElement;
    this.markButtons = document.querySelectorAll('[data-toolbar-mark]');
    this.typeButtons = document.querySelectorAll('[data-toolbar-type]');

    // Focus the editor on any click in the toolbar because we don't want to treat that as a blur
    this.preventFocus = (e) => {
      e.preventDefault();
      editorView.focus();
    };

    this.toolbar.addEventListener('mousedown', this.preventFocus);
    this.markButtons.forEach((button: HTMLElement) => {
      button.addEventListener('click', () => {
        if (button.dataset.toolbarMark === 'link') {
          editorView.dispatch(
            editorView.state.tr.setMeta(linkInspectorPluginKey, {
              newLink: {
                $start: editorView.state.selection.$from,
                $end: editorView.state.selection.$to,
              },
            })
          );
        } else {
          toggleMark(documentSchema.marks[button.dataset.toolbarMark!])(
            editorView.state
          );
        }
      });
    });
    this.typeButtons.forEach((button) => {
      button.addEventListener('click', () => {
        if (button.dataset.active === 'true') {
          setBlockType(documentSchema.nodes.paragraph)(
            editorView.state,
            editorView.dispatch
          );
        } else {
          if (button.dataset.toolbarType === 'ordered_list') {
            wrapInOrderedList(editorView.state, editorView.dispatch);
          } else if (button.dataset.toolbarType === 'unordered_list') {
            wrapInUnorderedList(editorView.state, editorView.dispatch);
          } else {
            setBlockType(documentSchema.nodes[button.dataset.toolbarType!])(
              editorView.state,
              editorView.dispatch
            );
          }
        }
      });
    });
    this.update(editorView);
  }

  update(editorView: EditorView<DocumentSchema>) {
    const { focus, activeMarks, activeType } = toolbarPluginKey.getState(
      editorView.state
    );

    if (focus) {
      this.toolbar.classList.remove('disabled');
    } else {
      this.toolbar.classList.add('disabled');
    }

    this.markButtons.forEach((button) => {
      button.classList.remove('active');
    });

    activeMarks.forEach((mark: MarkType<DocumentSchema>) => {
      const markButton = document.querySelector(
        `[data-toolbar-mark="${mark.name}"]`
      );
      if (markButton) {
        markButton.classList.add('active');
      }
    });

    this.typeButtons.forEach((button) => {
      button.classList.remove('active');
      button.dataset.active = undefined;
    });

    if (activeType) {
      const activeTypeButton = document.querySelector(
        `[data-toolbar-type="${activeType.name}"]`
      ) as HTMLElement;
      if (activeTypeButton) {
        activeTypeButton.classList.add('active');
        activeTypeButton.dataset.active = 'true';
      }
    }
  }

  destroy() {
    this.toolbar.removeEventListener('mousedown', this.preventFocus);
  }
}
