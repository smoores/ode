import { Plugin } from 'prosemirror-state';
import ToolbarView from './toolbarView';
import { toolbarPluginKey } from './pluginKey';
import { documentSchema } from '../../../../schema';

export default new Plugin({
  key: toolbarPluginKey,
  state: {
    init() {
      return { focus: false, activeMarks: [], activeType: null };
    },
    apply(tr) {
      const focus = tr.getMeta(toolbarPluginKey);
      const { from, $from, to, empty } = tr.selection;
      const activeMarks = empty
        ? (tr.storedMarks || $from.marks()).map((mark) => mark.type)
        : Object.values(documentSchema.marks).filter((mark) =>
            tr.doc.rangeHasMark(from, to, mark)
          );
      const activeType = $from.parent.type;
      return { focus, activeMarks, activeType };
    },
  },
  view(editorView) {
    return new ToolbarView(editorView);
  },
  props: {
    handleDOMEvents: {
      blur(editorView) {
        editorView.dispatch(
          editorView.state.tr.setMeta(toolbarPluginKey, { focus: false })
        );
        return true;
      },
      focus(editorView) {
        editorView.dispatch(
          editorView.state.tr.setMeta(toolbarPluginKey, { focus: true })
        );
        return true;
      },
    },
  },
});
