import { PluginKey } from 'prosemirror-state';

export const toolbarPluginKey = new PluginKey('toolbar');
