import { getCurrentDocument } from './editing';
import { ACL, Query, User } from 'parse';
import { getCurrentUser } from './auth';

export function makeDocumentPublic() {
  const doc = getCurrentDocument();
  const publicACL = new ACL();
  publicACL.setPublicReadAccess(true);
  publicACL.setPublicWriteAccess(true);
  doc.save({
    ACL: publicACL,
  });
}

export async function shareDocumentWithUsers(usernames: string[]) {
  const user = getCurrentUser();
  if (!user) {
    return;
  }
  const doc = getCurrentDocument();
  const users = (await new Query('User')
    .containedIn('username', usernames)
    .find()) as User[];
  const newACL = new ACL();
  newACL.setReadAccess(user, true);
  newACL.setWriteAccess(user, true);
  users.forEach((user: User) => {
    newACL.setReadAccess(user, true);
    newACL.setWriteAccess(user, true);
  });
  doc.save({
    ACL: newACL,
    sharedWith: users,
  });
}

export async function getAllUsers() {
  const usersQuery = new Query('User');
  return usersQuery.find();
}
