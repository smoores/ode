import { Query } from 'parse';

export async function getDocumentsByUser(user: Parse.User) {
  const ownedDocs = new Parse.Query('Document').equalTo('owner', user);
  const sharedDocs = new Parse.Query('Document').equalTo('sharedWith', user);
  const query = Query.or(ownedDocs, sharedDocs);
  const documents = await query.find();
  await Promise.all(documents.map((doc) => doc.get('owner').fetch()));
  return documents;
}
