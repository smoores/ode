import * as Parse from 'parse';

export function initialize() {
  window.Parse = Parse;
  Parse.initialize(window.APP_ID!);
  // Parse incorrectly defines serverURL as readonly -_-
  (Parse as {
    serverURL: string;
  }).serverURL = `${window.PROTOCOL}${window.DOMAIN}/parse`;

  const currentUser = Parse.User.current();
  if (currentUser && window.location.pathname === '/login') {
    window.location.pathname = '/';
  }
  if (!currentUser && window.location.pathname !== '/login') {
    window.location.pathname = '/login';
  }
}
