import * as Parse from 'parse';

export async function signup(email: string, password: string) {
  const user = new Parse.User();

  try {
    await user.signUp({
      username: email,
      password,
      email,
    });
  } catch (e) {
    console.error(e);
  }
}

export function login(email: string, password: string) {
  try {
    return Parse.User.logIn(email, password);
  } catch (e) {
    console.error(e);
  }
}

export async function logout() {
  await Parse.User.logOut();
  window.location.href = '/';
}

export function getCurrentUser() {
  return Parse.User.current();
}
