import { Query, Object as ParseObject } from 'parse';
import { getCurrentUser } from './auth';

interface Read {
  results: Array<Record<string, unknown>>;
  query: Record<string, unknown>;
  className: string;
  type: string;
}

class Transaction {
  private reads: Read[] = [];
  private writes: ParseObject[] = [];
  private sessionToken?: string;

  constructor() {
    const user = getCurrentUser();
    if (!user) {
      console.error("Can't create Transaction; no valid user session");
      return;
    }
    this.sessionToken = user.getSessionToken();
  }

  Query(objectClass: string) {
    const thisTransaction = this;
    const query = new Query(objectClass);
    const oldGet = query.get.bind(query) as typeof Query.prototype.get;
    query.get = function(objectId, ...getArgs) {
      return oldGet(objectId, ...getArgs).then((result: ParseObject) => {
        const results = result ? [result] : [];
        thisTransaction.reads.push({
          results: results.map((r) => ({
            ...r.toJSON(),
            __type: 'Object',
            className: this.className,
          })),
          query: this.toJSON(),
          className: this.className,
          type: 'get',
        });
        return result;
      });
    };
    const oldFirst = query.first.bind(query) as typeof Query.prototype.first;
    query.first = function(objectId, ...firstArgs) {
      return oldFirst(objectId, ...firstArgs).then((result: ParseObject) => {
        const results = result ? [result] : [];
        thisTransaction.reads.push({
          results: results.map((r) => ({
            ...r.toJSON(),
            __type: 'Object',
            className: this.className,
          })),
          query: this.toJSON(),
          className: this.className,
          type: 'first',
        });
        return result;
      });
    };
    const oldFind = query.find.bind(query) as typeof Query.prototype.find;
    query.find = function(...findArgs) {
      return oldFind(...findArgs).then((results: ParseObject[]) => {
        thisTransaction.reads.push({
          results: results.map((r) => ({
            ...r.toJSON(),
            __type: 'Object',
            className: this.className,
          })),
          query: this.toJSON(),
          className: this.className,
          type: 'find',
        });
        return results;
      });
    };
    return query;
  }

  save(parseObject: ParseObject): void;
  save(parseObjects: ParseObject[]): void;
  save(parseObjects: ParseObject | ParseObject[]): void {
    if (!Array.isArray(parseObjects)) {
      parseObjects = [parseObjects];
    }
    this.writes.push(...parseObjects);
  }

  toJSON() {
    return {
      sessionToken: this.sessionToken,
      reads: this.reads,
      writes: this.writes.map((w) => ({
        ...w.toJSON(),
        __type: 'Object',
        className: w.className,
      })),
    };
  }
}

async function run(transactional: (t: Transaction) => Promise<void>) {
  let success = false;
  while (!success) {
    const transaction = new Transaction();
    try {
      await transactional(transaction);
    } catch (e) {
      console.warn(e.message);
      return;
    }
    ({ success } = await (
      await fetch(`${window.PROTOCOL}${window.DOMAIN}/transaction`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(transaction.toJSON()),
      })
    ).json());
    if (!success) {
      console.warn('Retrying transaction');
    }
  }
}

export const ParseTransaction = {
  run,
};
