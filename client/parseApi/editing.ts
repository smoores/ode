import { Object as ParseObject, Attributes, Query, ACL } from 'parse';
import { Node } from 'prosemirror-model';
import { Step as ProsemirrorStep, Transform } from 'prosemirror-transform';
import { DocumentSchema, documentSchema } from '../../schema';
import { getCollabAuthority } from '../prosemirror/initialize';
import { ParseTransaction } from './transactions';
import { getCurrentUser } from './auth';
import { EditorState } from 'prosemirror-state';
import { sendableSteps } from 'prosemirror-collab';
import { Semaphore } from 'prex';

interface ParseClassConstructor {
  new <T extends Attributes>(
    className: string,
    attributes: T,
    options?: Record<string, unknown>
  ): ParseObject<T>;
  new (
    className?: string,
    attributes?: Attributes,
    options?: Record<string, unknown>
  ): ParseObject;
}

const Document = ParseObject.extend('Document') as ParseClassConstructor;
const Step = ParseObject.extend('Step') as ParseClassConstructor;

let currentDocument = new Document() as ParseObject;

export async function initializeDocument(doc: Node<DocumentSchema>) {
  const docId = await (await fetch('/words')).text();
  await currentDocument.save({
    docId,
    docJSON: doc.toJSON(),
    owner: getCurrentUser(),
    ACL: new ACL(getCurrentUser()),
  });
  return currentDocument;
}

function setCurrentDocument(nextCurrentDocument: ParseObject | undefined) {
  if (nextCurrentDocument) {
    currentDocument = nextCurrentDocument;
  }
}

export function getCurrentDocument() {
  return currentDocument;
}

const mutex = new Semaphore(1);

export async function queryForNewSteps() {
  await mutex.wait();
  const newStepsQuery = new Query('Step');
  newStepsQuery.equalTo('doc', currentDocument);
  newStepsQuery.greaterThan('stepId', getCollabAuthority().version);
  newStepsQuery.addAscending('stepId');
  newStepsQuery.limit(10000);
  console.debug('Requesting all steps after', getCollabAuthority().version);
  const newSteps = await newStepsQuery.find();
  const [receivedSteps, clientIds] = newSteps.reduce<
    [Array<ProsemirrorStep<DocumentSchema>>, string[]]
  >(
    ([steps, clients], parseStep) => {
      const step = ProsemirrorStep.fromJSON(
        documentSchema,
        parseStep.get('stepJSON')
      );
      const clientID = parseStep.get('clientID');
      console.debug(
        'Receiving new step',
        parseStep.get('stepId'),
        'from',
        clientID
      );
      return [
        [...steps, step],
        [...clients, clientID],
      ];
    },
    [[], []]
  );
  if (receivedSteps.length) {
    console.debug('Received steps', receivedSteps);
    getCollabAuthority().receiveSteps(
      newSteps[0].get('stepId'),
      receivedSteps,
      clientIds
    );
  }
  mutex.release();
}

export async function getDocumentState(docId: string) {
  setCurrentDocument(
    await new Query('Document').equalTo('docId', docId).first()
  );
  const initialDoc = documentSchema.nodeFromJSON(
    currentDocument.get('docJSON')
  );
  const newStepsQuery = new Query('Step');
  newStepsQuery.equalTo('doc', currentDocument);
  newStepsQuery.greaterThan('stepId', currentDocument.get('lastStepId') || 0);
  newStepsQuery.addAscending('stepId');
  newStepsQuery.limit(10000);
  const newSteps = await newStepsQuery.find();
  const stepsForCurrentDocument = new Query('Step');
  stepsForCurrentDocument.equalTo('doc', currentDocument);
  const newStepsSubscription = await stepsForCurrentDocument.subscribe();
  newStepsSubscription.on('create', queryForNewSteps);
  if (!newSteps.length) {
    return { doc: initialDoc, version: currentDocument.get('lastStepId') || 0 };
  }
  return {
    doc: newSteps.reduce(
      (tr, step) =>
        tr.step(ProsemirrorStep.fromJSON(documentSchema, step.get('stepJSON'))),
      new Transform(initialDoc)
    ).doc,
    version: newSteps[newSteps.length - 1].get('stepId'),
  };
}

export async function writeSteps(state: EditorState<DocumentSchema>) {
  const sendable = sendableSteps(state);
  if (!sendable) {
    return false;
  }
  const { version, steps, clientID } = sendable;
  console.debug(
    'Attempting to write steps',
    steps,
    'starting from version',
    version
  );
  await ParseTransaction.run(async (t) => {
    let stepCount = version + 1;
    const stepQuery = t.Query('Step');
    stepQuery.equalTo('stepId', stepCount);
    stepQuery.equalTo('doc', currentDocument);
    const existingStep = await stepQuery.first();
    if (existingStep) {
      throw new Error(`There is already a step with stepId ${stepCount}`);
    }
    const parseSteps = steps.map((step) => {
      const parseStep = new Step() as ParseObject;
      parseStep.set('stepId', stepCount++);
      parseStep.set('stepJSON', step.toJSON());
      parseStep.set('doc', currentDocument);
      parseStep.set('clientID', clientID);
      parseStep.set('author', getCurrentUser());
      return parseStep;
    });
    console.debug(
      'Writing new steps with ids',
      parseSteps.map((s) => s.get('stepId')),
      'from',
      clientID
    );
    console.debug(steps);
    t.save(parseSteps);
  });
  return true;
}

export async function updateDocumentTitle(title: string) {
  currentDocument.save({ title });
}
