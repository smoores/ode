import { updateDocumentTitle } from '../parseApi/editing';

export function renderDocumentTitle(title: string) {
  document.querySelector<HTMLElement>(
    '[data-document-title]'
  )!.innerText = title;
  const titleComputer = document.querySelector<HTMLElement>(
    '[data-document-title-computer]'
  )!;
  titleComputer.innerText = title;
  const width = titleComputer.getBoundingClientRect().width;
  titleComputer.style.left = `-${width}px`;
}

function handleDocumentTitleFocus() {
  const titleSpan = document.querySelector<HTMLElement>(
    '[data-document-title]'
  )!;
  const titleComputer = document.querySelector<HTMLElement>(
    '[data-document-title-computer]'
  )!;
  const titleInput = document.querySelector<HTMLInputElement>(
    '[data-document-title-input]'
  )!;
  titleInput.hidden = false;
  titleInput.removeAttribute('aria-hidden');
  titleSpan.hidden = true;
  titleSpan.setAttribute('aria-hidden', '');

  const width = titleComputer.getBoundingClientRect().width;

  titleInput.value = titleSpan.innerText;
  titleInput.style.width = `${width}px`;
  titleInput.focus();
  titleInput.setSelectionRange(
    titleInput.value.length,
    titleInput.value.length
  );
}

async function handleDocumentTitleBlur() {
  const titleSpan = document.querySelector<HTMLElement>(
    '[data-document-title]'
  )!;
  const titleInput = document.querySelector<HTMLInputElement>(
    '[data-document-title-input]'
  )!;
  titleInput.hidden = true;
  titleInput.setAttribute('aria-hidden', '');
  titleSpan.hidden = false;
  titleSpan.removeAttribute('aria-hidden');

  await updateDocumentTitle(titleInput.value);
  titleSpan.innerText = titleInput.value;
}

function handleDocumentTitleKeyDown(e: KeyboardEvent) {
  if (e.key === 'Enter') {
    handleDocumentTitleBlur();
  }
}

function handleDocumentTitleChange() {
  const titleComputer = document.querySelector<HTMLElement>(
    '[data-document-title-computer]'
  )!;
  const titleInput = document.querySelector<HTMLInputElement>(
    '[data-document-title-input]'
  )!;
  titleComputer.innerText = titleInput.value;
  const width = titleComputer.getBoundingClientRect().width;
  titleComputer.style.left = `-${width}px`;

  titleInput.style.width = `${width}px`;
}

export function bindDocumentTitle() {
  const titleSpan = document.querySelector<HTMLSpanElement>(
    '[data-document-title]'
  )!;
  titleSpan.addEventListener('focus', handleDocumentTitleFocus);
  const titleInput = document.querySelector<HTMLInputElement>(
    '[data-document-title-input]'
  )!;
  titleInput.addEventListener('blur', handleDocumentTitleBlur);
  titleInput.addEventListener('keydown', handleDocumentTitleKeyDown);
  titleInput.addEventListener('input', handleDocumentTitleChange);
}
