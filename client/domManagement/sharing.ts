import { openDialog, closeDialog } from './dialog';
import {
  makeDocumentPublic,
  getAllUsers,
  shareDocumentWithUsers,
} from '../parseApi/sharing';

async function handleSharedWithSubmit(event: Event) {
  event.preventDefault();
  const username = document.querySelector<HTMLInputElement>(
    '[data-shared-with-input]'
  )!.value;
  const sharedList = document.querySelector<HTMLInputElement>(
    '[data-shared-with-list]'
  )!;
  const newListItem = document.createElement('li');
  newListItem.innerText = username;
  sharedList.appendChild(newListItem);
}

async function handleSharingSettingsSubmit(
  event: Event,
  dialog: HTMLDialogElement
) {
  event.preventDefault();
  const isPublic = document.querySelector<HTMLInputElement>(
    '[data-share-public]'
  )!.checked;
  if (isPublic) {
    makeDocumentPublic();
  } else {
    const sharedList = document.querySelector<HTMLUListElement>(
      '[data-shared-with-list]'
    )!;
    const usernames = Array.from(sharedList.children).map(
      (listItem) => (listItem as HTMLLIElement).innerText
    );
    await shareDocumentWithUsers(usernames);
  }
  closeDialog(dialog);
}

export function bindShareButton() {
  const shareButton = document.querySelector<HTMLButtonElement>(
    '[data-share-button]'
  )!;
  const shareDialog = document.querySelector<HTMLDialogElement>(
    '[data-share-dialog]'
  )!;
  shareButton.addEventListener('click', () => openDialog(shareDialog));
}

export function bindShareDialogForm() {
  const shareDialog = document.querySelector<HTMLDialogElement>(
    '[data-share-dialog]'
  )!;
  const shareForm = shareDialog.querySelector<HTMLFormElement>(
    '[data-sharing-settings-form]'
  )!;
  const cancelButton = shareDialog.querySelector<HTMLButtonElement>(
    '[data-close]'
  )!;
  shareForm.addEventListener('submit', (event) =>
    handleSharingSettingsSubmit(event, shareDialog)
  );
  cancelButton.addEventListener('click', () => closeDialog(shareDialog));
}

export async function bindUsersList() {
  const userDataList = document.querySelector(
    '[data-user-list]'
  ) as HTMLDataListElement;
  const users = await getAllUsers();
  users.forEach((user) => {
    const option = document.createElement('option');
    option.value = user.get('username');
    userDataList.appendChild(option);
  });
}

export function bindSharedWithForm() {
  const sharedWithForm = document.querySelector(
    '[data-shared-with-form]'
  ) as HTMLFormElement;
  sharedWithForm.addEventListener('submit', handleSharedWithSubmit);
}
