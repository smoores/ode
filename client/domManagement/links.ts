import { EditorView } from 'prosemirror-view';
import { TextSelection } from 'prosemirror-state';
import { documentSchema } from '../../schema';

function insertEditWidget(
  inspector: HTMLDivElement,
  editorView: EditorView,
  getPos: () => number,
  getSize: () => number,
  currentHref?: string
) {
  // TODO: Might be able to use a <template> here instead!
  inspector.innerHTML = `
    <form>
      <input id="edit-href" name="edit-href" type="text" ${currentHref &&
        `value="${currentHref}"`}>
      <button class="primary" type="submit">Save</button>
    </form>
  `;
  setTimeout(() => inspector.querySelector('input')!.focus());
  inspector
    .querySelector<HTMLFormElement>('form')!
    .addEventListener('submit', (e) => {
      e.preventDefault();
      const formData = new FormData(e.target as HTMLFormElement);
      const tr = editorView.state.tr;
      const startPos = getPos();
      const endPos = startPos + getSize();
      editorView.dispatch(
        tr
          .removeMark(startPos, endPos, documentSchema.marks.link)
          .addMark(
            startPos,
            endPos,
            documentSchema.marks.link.create({
              href: formData.get('edit-href'),
            })
          )
          .setSelection(TextSelection.create(tr.doc, endPos))
      );
      editorView.focus();
    });
}

function createInspector() {
  const target = document.createElement('span');
  target.classList.add('link-inspector-target');
  target.dataset.linkInspectorTarget = 'true';
  const inspector = document.createElement('div');
  inspector.classList.add('link-inspector');
  inspector.addEventListener('keydown', (e) => {
    e.stopPropagation();
  });
  target.appendChild(inspector);
  return target;
}

export function createNewLinkInspector(
  editorView: EditorView,
  getPos: () => number,
  getSize: () => number
) {
  const newLinkInspectorTarget = createInspector();
  const newLinkInspector = newLinkInspectorTarget.firstElementChild as HTMLDivElement;
  insertEditWidget(newLinkInspector, editorView, getPos, getSize);
  return newLinkInspectorTarget;
}

export function createEditLinkInspector(
  editorView: EditorView,
  getPos: () => number,
  getSize: () => number,
  currentHref: string
) {
  const editLinkInspectorTarget = createInspector();
  const editLinkInspector = editLinkInspectorTarget.firstElementChild as HTMLDivElement;
  editLinkInspector.innerHTML = `
    <span>${currentHref}</span>
    <button class="primary" data-inspect-link-edit-button type="button">Edit</button>
    <button class="text-only" data-inspect-link-remove-button type="button">Remove</button>
  `;
  editLinkInspector
    .querySelector<HTMLButtonElement>('[data-inspect-link-remove-button]')!
    .addEventListener('click', (e) => {
      e.preventDefault();
      const tr = editorView.state.tr;
      const startPos = getPos();
      const endPos = startPos + getSize();
      console.log(startPos, endPos, documentSchema.marks.link);
      editorView.dispatch(
        tr
          .removeMark(startPos, endPos, documentSchema.marks.link)
          .setSelection(TextSelection.create(tr.doc, endPos))
      );
      editorView.focus();
    });
  editLinkInspector
    .querySelector<HTMLButtonElement>('[data-inspect-link-edit-button]')!
    .addEventListener('click', () => {
      insertEditWidget(
        editLinkInspector,
        editorView,
        getPos,
        getSize,
        currentHref
      );
    });
  return editLinkInspectorTarget;
}
