import 'wicg-inert';

export function openDialog(dialog: HTMLDialogElement) {
  dialog.setAttribute('open', '');
  dialog.removeAttribute('hidden');
  const focusLock = document.querySelector<HTMLElement>('[data-focus-lock]')!;
  Array.from(document.body.children)
    .filter((child) => child !== dialog && child !== focusLock)
    .forEach((child) => {
      child.remove();
      focusLock.appendChild(child);
    });
  document.body.classList.remove('layout');
  focusLock.classList.add('layout');
}

export function closeDialog(dialog: HTMLDialogElement) {
  dialog.removeAttribute('open');
  dialog.setAttribute('hidden', '');
  const focusLock = document.querySelector<HTMLElement>('[data-focus-lock]')!;
  Array.from(focusLock.children)
    .reverse()
    .forEach((child) => {
      child.remove();
      document.body.prepend(child);
    });
  focusLock.classList.remove('layout');
  document.body.classList.add('layout');
}
