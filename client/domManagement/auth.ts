import { signup, login } from '../parseApi/auth';

async function handleSignupSubmit(event: Event) {
  event.preventDefault();
  const email = (event.target as Element).querySelector<HTMLInputElement>(
    '[data-email]'
  )!.value;
  const password = (event.target as Element).querySelector<HTMLInputElement>(
    '[data-password]'
  )!.value;
  await signup(email, password);
  window.location.pathname = '/';
}

async function handleLoginSubmit(event: Event) {
  event.preventDefault();
  const email = (event.target as Element).querySelector<HTMLInputElement>(
    '[data-email]'
  )!.value;
  const password = (event.target as Element).querySelector<HTMLInputElement>(
    '[data-password]'
  )!.value;
  await login(email, password);
  window.location.pathname = '/';
}

export function bindSignup() {
  const authForm = document.querySelector('[data-auth-form]')!;
  authForm.removeEventListener('submit', handleLoginSubmit);
  authForm.addEventListener('submit', handleSignupSubmit);
}

export function bindLogin() {
  const authForm = document.querySelector('[data-auth-form]')!;
  authForm.removeEventListener('submit', handleSignupSubmit);
  authForm.addEventListener('submit', handleLoginSubmit);
}

function toggleSignupLogin() {
  const authForm = document.querySelector<HTMLElement>('[data-auth-form]')!;
  const loginButtons = document.querySelector<HTMLElement>(
    '[data-login-buttons]'
  )!;
  const signupButtons = document.querySelector<HTMLElement>(
    '[data-signup-buttons]'
  )!;
  const passwordInput = document.querySelector<HTMLInputElement>(
    '[data-password]'
  )!;
  switch (authForm.dataset.authForm) {
    case 'signup': {
      authForm.dataset.authForm = 'login';
      loginButtons.hidden = false;
      loginButtons.setAttribute('aria-hidden', 'false');
      signupButtons.hidden = true;
      signupButtons.setAttribute('aria-hidden', 'true');
      passwordInput.autocomplete = 'current-password';
      bindLogin();
      break;
    }
    case 'login': {
      authForm.dataset.authForm = 'signup';
      signupButtons.hidden = false;
      signupButtons.setAttribute('aria-hidden', 'false');
      loginButtons.hidden = true;
      loginButtons.setAttribute('aria-hidden', 'true');
      passwordInput.autocomplete = 'new-password';
      bindSignup();
      break;
    }
    default: {
      return;
    }
  }
}

export function bindToggle() {
  document
    .querySelectorAll<HTMLElement>('[data-auth-form-toggle]')
    .forEach((toggle) => {
      toggle.addEventListener('click', toggleSignupLogin);
    });
}
