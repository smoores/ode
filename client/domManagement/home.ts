import { Object as ParseObject } from 'parse';
import { formatDistanceStrict, isAfter } from 'date-fns';

function fromNow(otherDate: number) {
  const now = Date.now();
  const relativeString = formatDistanceStrict(now, otherDate);
  return isAfter(otherDate, now)
    ? `in ${relativeString}`
    : `${relativeString} ago`;
}

const documentTemplate = (doc: ParseObject) => `
  <tr>
    <td>
      <a href="/editing/${doc.get('docId')}">
        ${doc.get('title') || doc.get('docId')}
      </a>
    </td>
    <td>
      ${fromNow(doc.get('updatedAt'))}
    </td>
    <td>
      ${fromNow(doc.get('createdAt'))}
    </td>
    <td>
      ${doc.get('owner').get('username')}
    </td>
  </tr>
`;

export function renderDocuments(documents: ParseObject[]) {
  documents
    .sort((a, b) => b.get('updatedAt') - a.get('updatedAt'))
    .forEach((doc) => {
      document
        .querySelector<HTMLElement>('tbody')!
        .insertAdjacentHTML('beforeend', documentTemplate(doc));
    });
}

function handleNewDocumentSubmit(e: Event) {
  e.preventDefault();
  window.location.href = '/create';
}

export function bindNewDocumentButton() {
  const newDocumentForm = document.querySelector<HTMLFormElement>(
    '[data-new-document-form]'
  )!;
  newDocumentForm.addEventListener('submit', handleNewDocumentSubmit);
}
