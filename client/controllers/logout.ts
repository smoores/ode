import { initialize } from '../parseApi/initialize';
import { logout } from '../parseApi/auth';

initialize();
logout();
