import {
  initializeDocument,
  getDocumentState,
  getCurrentDocument,
} from '../parseApi/editing';
import { initialize } from '../parseApi/initialize';
import {
  renderDocumentTitle,
  bindDocumentTitle,
} from '../domManagement/header';
import {
  initializeEditorState,
  initializeEditorView,
} from '../prosemirror/initialize';
import {
  bindShareButton,
  bindShareDialogForm,
  bindUsersList,
  bindSharedWithForm,
} from '../domManagement/sharing';

const createRegex = /\/create/;
const editingRegex = /\/editing\/((?:[a-z\d]{4,8}\.){3}[a-z\d]{4,8})/i;

async function initializePage(): Promise<void> {
  if (location.pathname.match(createRegex)) {
    const state = initializeEditorState();
    initializeDocument(state.doc).then((currentDocument) => {
      window.history.pushState(
        null,
        'New Document',
        `/editing/${currentDocument.get('docId')}`
      );
      initializePage();
    });
    return;
  }
  const documentIdMatch = location.pathname.match(editingRegex);
  if (!documentIdMatch) {
    window.history.pushState(null, 'Create Document', `/create`);
    return initializePage();
  }

  const { doc, version } = await getDocumentState(documentIdMatch[1]);
  const view = initializeEditorView(doc, version);
  window.view = view;
  const currentDocument = getCurrentDocument();
  renderDocumentTitle(
    currentDocument.get('title') || currentDocument.get('docId')
  );
  bindDocumentTitle();
  bindShareButton();
  bindShareDialogForm();
  bindUsersList();
  bindSharedWithForm();
}

initialize();
initializePage();
