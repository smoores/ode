import { initialize } from '../parseApi/initialize';
import { getCurrentUser } from '../parseApi/auth';
import { getDocumentsByUser } from '../parseApi/documents';
import { renderDocuments, bindNewDocumentButton } from '../domManagement/home';

async function getDocuments() {
  const user = getCurrentUser();
  if (!user) {
    return;
  }
  const documents = await getDocumentsByUser(user);
  renderDocuments(documents);
}

initialize();
getDocuments();
bindNewDocumentButton();
