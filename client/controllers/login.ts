import { initialize } from '../parseApi/initialize';
import { bindSignup, bindToggle } from '../domManagement/auth';

initialize();
bindSignup();
bindToggle();
