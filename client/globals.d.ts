import { EditorView } from 'prosemirror-view';

declare global {
  interface Window {
    view: EditorView;
    PROTOCOL: string;
    DOMAIN: string;
    APP_ID: string;
  }
}
