const path = require('path');
const BrotliPlugin = require('brotli-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const ForkTsCheckerNotifierWebpackPlugin = require('fork-ts-checker-notifier-webpack-plugin');

module.exports = {
  entry: {
    editing: './client/controllers/editing.ts',
    login: './client/controllers/login.ts',
    home: './client/controllers/home.ts',
    logout: './client/controllers/logout.ts',
  },
  output: {
    path: path.resolve(__dirname, 'public/assets/js/lib'),
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
          options: { transpileOnly: true },
        },
      },
      {
        test: /\.css$/,
        use: {
          loader: 'css-loader',
        },
      },
    ],
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin({ eslint: { files: './**/*.{ts,js}' } }),
    new ForkTsCheckerNotifierWebpackPlugin({
      title: 'Typescript',
      excludeWarnings: false,
    }),
    new BrotliPlugin({
      asset: '[path].br[query]',
      test: /\.(js|css|html|svg)$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
  ],
  resolve: {
    extensions: ['.ts', '.js', '.json'],
    fallback: {
      crypto: false,
      stream: false,
      util: require.resolve('util/'),
    },
  },
  devtool: 'eval-source-map',
};
